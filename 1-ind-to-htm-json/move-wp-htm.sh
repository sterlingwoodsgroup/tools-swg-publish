#!/bin/bash
#requires wp-cli.yml to be setup

#get generated post data, get featured_image, categories via JSON

src='/Users/andregagnon/Sites/byo3/indesign-output';
cd $src;

#dir='./indesign-output';

#move images to website
dest='/Users/andregagnon/Sites/wm3/wm3/images/indq-temp';
#find * | grep  "\.jpg" | xargs -I {} cp {} $dest
find * | grep  "\.jpg" | xargs -I {} -t cp "{}" $dest
find * | grep  "\.png" | xargs -I {} -t cp "{}" $dest

#move data to destination
dest='/Users/andregagnon/Sites/wm3/indesign-output';

cp *.wp.htm $dest
cp *.json $dest

echo $dest;

exit


#cleanup

rm *.wp.htm
rm *.html
rm *.json
rm *.indd

cp -r * $dest

echo $1 $2 $3 $4


# wp media regenerate --yes
# wp post list --topic=importe
