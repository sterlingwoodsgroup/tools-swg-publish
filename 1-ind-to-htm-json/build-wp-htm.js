
// Globals
const jsdom = require("jsdom");
var fs = require("fs");
var path = require("path");
const { JSDOM } = jsdom;

// set up some string constants
// var eol = '\r\n\r\n';
var eol = '\r\n';
var out = '';

// list of html files
var htmlList = [];

// structure
var import_type = 'InDesign';
var issue_date = 'April-May 2018';
var topic = 'Departments';

//
// Helper functions
//

function extension( element) {
  var ext = path.extname( element);
  return( ext == '.' + 'html');
}

function baseFilename( element ) {
   return( path.basename( element, '.html'));
}


// recursive
// traverse all children in list and output
// modify some of the output
function traverseDOM( list, base) {

  //console.log( list);
  for ( var child of list) {

    // console.log( child);

    // console.log( child);
    if ( child.nodeType == 1) {
      // console.log( child.nodeName);

      // add line breaks
      if( child.nodeName == 'P' ) {
        out += eol;
      }
      if( child.nodeName == 'DIV' ) {
        if ( child.innerText)
          out += eol;
      }

      // modify output
      if( child.nodeName == 'IMG') {
        if ( !child.outerHTML.includes('data:image'))   {
          child.removeAttribute( 'class');
          child.setAttribute( 'class', 'aligncenter');

          var src = child.getAttribute( 'src');
          // console.log( 'src', src);
          // console.log( 'base', base);
          if ( src ) {

            var newSrc = src.replace( base+'-web-resources/image', '/images/indq').replace('(','').replace(')','');
            // console.log( 'newSrc', newSrc);
            child.setAttribute( 'src', newSrc);
            console.log( '*** src-replace: ',  base+'-web-resources/image');
            console.log( '^^^ src-new: ',  newSrc);

          }

          out += child.outerHTML + eol;
        }
      }
      else if(  child.nodeName == 'A') {
        out += child.outerHTML + eol;
      }
      //else if( child.nodeName == 'P') {
      else if( child.nodeName == 'SPAN') {
        // console.log( child );
        // for( var prop in child ) {
        //    console.log( child, prop, child[prop]);
        //  }
        //out += child.innerText + eol;
        // str = child.innerHTML.replace( '\/t', '');
        // out += str + eol;
        out += child.innerHTML.replace( '\t', '') + '';
        // out += child.innerHTML + eol;
        //out += child.textContent + eol;
      }
      else if ( child.children.length > 0)
        traverseDOM( child.children, base);
    }
  }
}

///////////////////////////////////////////////////////////////////////////////
// Build data
//

// global
var workingDir = './indesign-output/';

// Get gnarly HTML and convert to WP HTML-content ready for import
// read all files
fs.readdir( workingDir, function(err, data) {
  if (err) throw err;

  // console.log('dir >> \n', data, '\n');


  // find files with html extension
  data.filter( extension).forEach( function (value) {
    // console.log( value);
    htmlList.push( baseFilename(value));
  });
  htmlList.sort();

  console.log( 'list >>', htmlList);

  // for each html file
  for( var i = 0; i < htmlList.length; i++) {
    var value = htmlList[ i];

    var inFile = workingDir + value + '.html';
    var baseFile = workingDir + baseFilename( value ).replace( / /g, '_');
    console.log( 'process file ->', inFile, baseFile);
    out = '';

    var data = fs.readFileSync( inFile, 'utf8');

    // console.log( data);
    var file = inFile;
    var baseFile = baseFilename( file ).replace( / /g, '_');
    var outFile = workingDir + baseFile + '.wp.htm';
    console.log( 'file', file);
    console.log( 'baseFile', baseFile);
    console.log( 'outFile', outFile);

    var dom2 = new JSDOM( data, { incldeNodeLocation: true});

    // console.log( dom2.window.document.body);
    // console.log( dom2.window.document.body.children);

    var children = dom2.window.document.body.children;
    traverseDOM( children, baseFile);
    fs.writeFileSync( outFile, out);
    console.log( outFile, ' saved!');

  }

});

// Get data e.g. featured image from HTML, save as JSON
// read html files
fs.readdir( workingDir, function(err, data) {
  if (err) throw err;

  // console.log('dir >> \n', data, '\n');

  data.filter( extension).forEach( function (value) {
    // console.log( value);
    htmlList.push( baseFilename(value));
  });
  htmlList.sort();

  console.log( 'list >>', htmlList);

  // htmlList.forEach( function( value) {
  for( var i = 0; i < htmlList.length; i++) {
    var value = htmlList[ i];

    var inFile = workingDir + value + '.html';
    var baseFile = workingDir + baseFilename( value ).replace( / /g, '_');
    console.log( 'process file ->', inFile, baseFile);

    var data2 = fs.readFileSync( inFile, 'utf8');
    var dom2 = new JSDOM( data2, { incldeNodeLocation: true});
    var images = dom2.window.document.images;

    var post_type = 'article';
    if ( value.includes( 'Tip')
      || value.includes( 'Technique')
      || value.includes( 'Advanced')
      || value.includes( 'Kits')) {
      post_type = 'technique';
    }
    else if ( value.includes( 'Wizard')) {
      post_type = 'wine-wizard';

    }

    var topic = 'Departments';
    if ( value.includes( 'Tip'))
      topic = 'Winemaking-Tips';
    else if ( value.includes( 'Technique'))
      topic = 'Techniques';
    else if ( value.includes( 'Advanced'))
      topic = 'Winemaking-Science';
    else if ( value.includes( 'Varietal'))
      topic = 'Varietals-and-Wine-Styles';
    else if ( value.includes( 'Wizard'))
      topic = 'Wine-Wizard';
    else if ( value.includes( 'Kits'))
      topic = 'Techniques';

    // var issue_date = 'april-may-2018';
    // var import_type = 'InDesign';

    // set up taxonomies, post_types
    var config = {};
    config.post_type = post_type;
    config.import_type = import_type;
    config.month_year = issue_date;
    config.topic = topic;

    // get first image, set as featured_image
    config.featured_image = 'none';

    console.log( 'Look for featured image');
    var featured = false;
    for (var j = 0; j < images.length; j++) {
      img = images[ j];
      if ( !img.outerHTML.includes('data:image'))   {
        var src = img.getAttribute('src');
        console.log( src);
        if ( src) {

          // src =  path.basename( src).replace('(','').replace(')','');
          src =  path.basename( src);

          if ( featured === false) {
            featured = true;
            console.log( 'Featured Image >> ', src);
            config.featured_image = src;
          }
        }
      }
    }
    console.log( 'Save config ', config);
    var configOut = JSON.stringify( config, null, 2);
    var outFile = workingDir + baseFilename( value ).replace( / /g, '_') + '.json';

    fs.writeFileSync( outFile, configOut);
    console.log( ' ');
  }

});
