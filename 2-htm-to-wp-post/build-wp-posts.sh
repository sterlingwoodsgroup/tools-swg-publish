#!/bin/bash
#requires wp-cli.yml to be setup

#get generated post data, get featured_image, categories via JSON

# read JSON data
function readJSON {
  UNAMESTR=`uname`
  if [[ "$UNAMESTR" == 'Linux' ]]; then
    SED_EXTENDED='-r'
  elif [[ "$UNAMESTR" == 'Darwin' ]]; then
    SED_EXTENDED='-E'
  fi;

  VALUE=`grep -m 1 "\"${2}\"" ${1} | sed ${SED_EXTENDED} 's/^ *//;s/.*: *"//;s/",?//'`

  if [ ! "$VALUE" ]; then
    echo "Error: Cannot find \"${2}\" in ${1}" >&2;
    exit 1;
  else
    echo $VALUE ;
  fi;
}


#delete attached media
#wp post delete $(wp post list --post_type='attachment' --format=ids --post_parent=$(wp post list --post_type=post --import-type=indesign --format=ids))

# #delete imported posts
# wp post delete $(wp post list --post_type=post --import-type=indesign --format=ids) --force
# #delete unattached media
# wp post delete $(wp post list --post_type='attachment' --format=ids --post_parent=0)


#dir='/vagrant/indesign-output';
dir='./indesign-output';
count=0;

#for name in "${articles[@]}"
#	do
for entry in "$dir"/*.wp.htm
	do
		echo
		echo "Entry > $entry"
    nopath=${entry##*/}
    # echo "NoPath > $nopath"
		name=${entry%.*.*}
    # echo "Name > $name"
    #title=${name^}
    raw_title=${nopath%.*.*}
    title=${raw_title//_/ }
		echo "Title > $title"
    slug=${raw_title,,}
    # echo "Slug > $slug"
    slug2=${slug//[0-9-]/}
    # echo "Slug2 > $slug2"
    slug3=${slug2//[-]/}
    # echo "Slug3 > $slug3"
    slug4=${slug3#_}
    # echo "Slug4 > $slug4"
    slug5=${slug4//_/-}
    echo "Slug5 > $slug5"

    imagePath=${entry%.*.*}"-web-resources/image"
    # echo "ImagePath > $imagePath"

    featured=`readJSON $name.json featured_image` || exit 1;
    echo "Featured Image > $imagePath/$featured"
    featured_title=${featured%.*}
    echo "Featured Image Title > $featured_title"

    import_type=`readJSON $name.json import_type` || exit 1;
    # import_type=$2
    echo "Import Type > $import_type"
    month_year=`readJSON $name.json month_year` || exit 1;
    # month_year=$1
    echo "Date Issued > $month_year"

    post_type=`readJSON $name.json post_type` || exit 1;
    # post_type=$3
    echo "Type  > $post_type"

    topic=`readJSON $name.json topic` || exit 1;
    # post_type=$3
    echo "Topic  > $topic"

    title=$title"-"$month_year

    create="wp post create $entry --post_type=$post_type \
    --post_title=$title --post_name=$slug5 --post_status=publish --porcelain"
    echo $create;

    #break;

    postID=$(wp post create $entry --post_type=$post_type \
    --post_title="$title" --post_name=$slug5 --post_status=publish --porcelain)

    #add terms
    wp post term add $postID import-source "$import_type"
    wp post term add $postID date $month_year --by=slug
    wp post term add $postID writer "WineMaker Staff"
    wp post term add $postID topic "$topic"

    #
    # # add media
    # #featured image
    if [ "$featured" = "none" ]
      then
        echo "No featured image."
      else
        echo "featured: $imagePath/$featured"
      # wp media import $imagePath/$featured --title=$featured_title --post_id=$postID --featured_image
    fi

    # #other media
    # #wp media import /vagrant/import-data/$name/*.jpg
    #wp media import $imagePath/*.jpg --post_id=$postID
    #wp media import $imagePath/*.png --post_id=$postID


    count=$((count+1));
    echo $count;
    if [ $count -gt 30 ]
      then
        break;
    fi

done

# echo $1 $2 $3 $4


# wp media regenerate --yes
# wp post list --topic=importe
