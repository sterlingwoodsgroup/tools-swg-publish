#!/bin/bash
#requires wp-cli.yml to be setup

wp post delete $(wp post list --post_type=article --format=ids) --force
wp post delete $(wp post list --post_type=technique --format=ids) --force
wp post delete $(wp post list --post_type=wine-wizard --format=ids) --force
