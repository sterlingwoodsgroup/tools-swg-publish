#!/bin/bash
#requires wp-cli.yml to be setup
if [ "$1" == "" ]
	then
    echo "No year provided"
    exit 1
  else
    year=$1
fi

wp export --post_type=article --filename_format="$year-article.xml"
wp export --post_type=technique --filename_format="$year-technique.xml"
wp export --post_type=wine-wizard --filename_format="$year-wine-wizard.xml"
