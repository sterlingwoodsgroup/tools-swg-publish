##import workflow

if Quark, convert to InDesign files
Flatten Quark folders
mkdir qxp
find * -type f | xargs -I {} cp {} qxp
Use MarkZWare for Quark to InDesign,
Markzware > Q2ID > Convert ...  (Select Multiple documents)
InDesign Save All as InDesign Files
Cmd-Alt/Option-Shift-S  (ind folder)
Close All, Cmd-W

Generate HTML/CSS/Images from InDesign files
Open all InDesign files
Use InDesign batchconvert.jsxbin for InDesign conversion HTML/Images
http://www.kahrel.plus.com/indesign/batch_convert.html
Open Scripts Window: Cmd-Alt/Option-F11
Run Script batch-convert.jsbin
Choose output folder (HTML folder)
OK
Close All: Shift-Option-Cmd W

on local DEV byo3
Generate cleaned up HTML, and JSON data
Set json parameters, e.g. date 'apr-may-2012' in build-wp-html.js
(Run in vagrant)
$ node build-wp-htm.js

Move html and images to wm3
$ dev/move-wp-htm.sh
watch for image filename errors

.
on local DEV wm3
Build wp posts
$ dev/build-wp-posts.sh

Create export XML file
$  dev/build-export.sh 2018

Import XML into WordPress Staging Site
admin > tools > import > WordPress Importer

Move Images
SFTP /images/indq-temp > local:/images/indq and remote:/images/indq

remove post types, begin again.
$ dev/delete-posts.sh
